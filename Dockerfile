# buildenv is implicitly needed by runtime's OnBuild hooks; otherwise it could be removed
 FROM artefact.skao.int/ska-tango-images-pytango-builder:9.3.10 as buildenv
FROM artefact.skao.int/ska-tango-images-pytango-runtime:9.3.10 as runtime

LABEL \
      author="Rodrigo Tobar and Stephen Ord" \
      description="An image for the tango device that controls the CBF emulator" \
      license="BSD-3-Clause" \
      vendor="SKA Telescope" \
      org.skatelescope.team="YANDA" \
      org.skatelescope.website="https://gitlab.com/ska-telescope/ska-sdp-cbf-emulator-tango-device/"
ARG PYPI_REPOSITORY_URL=https://artefact.skao.int/repository/pypi-internal


ARG PYPI_REPOSITORY_URL=https://nexus.engageska-portugal.pt/repository/pypi

# Install our application, which is already present under /app
# (again, through an OnBuild hook in the runtime image)
RUN pip install --extra-index-url $PYPI_REPOSITORY_URL/simple /app

CMD ["/venv/bin/python", "-m", "ska.cbf.tango_device"]

