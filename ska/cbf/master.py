from ska_tango_base.control_model import AdminMode, HealthState
from ska_tango_base.master_device import SKAMaster
import tango
from tango.server import attribute, device_property


def _make_forwarding_command(command, allowed_state, forwards_to=None):
    """Creates a Command object that forwards the current command to CbfSubarray"""
    forwards_to = forwards_to or command
    target_state = tango.DevState.names[command.upper()]
    allowed_state = tango.DevState.names[allowed_state.upper()]
    def is_allowed(self):
        return self.dev_state() == allowed_state
    def do(self):
        self.set_state(target_state)
        self._subarrays.command_inout(forwards_to)
        self.info_stream(f"{command} command forwarded as {forwards_to} to subarrays")
    is_allowed.__name__ = f'is_{command}_allowed'
    do.__name__ = command
    return is_allowed, tango.server.command(do)

def _make_mock_reporting_attributes(device_type, max_devices, num_devices_property):
    """
    Creates the mock reporting attributes for a type of CBF subdevices, which
    are always ONLINE, healthy, ON and unassigned.
    """

    def admin(self):
        return [AdminMode.ONLINE] * getattr(self, num_devices_property)
    admin = attribute(
        name=f"report{device_type}AdminMode", dtype=('uint16',),
        max_dim_x=max_devices, label=f"{device_type} admin mode")(admin)

    def health_state(self):
        return [HealthState.OK] * getattr(self, num_devices_property)
    health_state = attribute(
        name=f"report{device_type}HealthState", dtype=('uint16',),
        max_dim_x=max_devices, label=f"{device_type} health status")(health_state)

    def state(self):
        return [tango.DevState.ON] * getattr(self, num_devices_property)
    state = attribute(
        name=f"report{device_type}State", dtype=('DevState',),
        max_dim_x=max_devices, label=f"{device_type} state")(state)

    def subarrayMembership(self):
        return [0] * getattr(self, num_devices_property)
    subarrayMembership = attribute(
        name=f"report{device_type}SubarrayMembership", dtype=('uint16',),
        max_dim_x=max_devices, label=f"{device_type} subarray membership")(subarrayMembership)

    return admin, health_state, state, subarrayMembership

class CbfMaster(SKAMaster):
    """
    A Tango device that looks like the real CbfMaster, but acts nothing like it.

    The only responsibility of this Master device is to forward its commands to
    all CbfSubarray devices like the real device does.
    """

    CbfSubarrays = device_property(dtype=('str',))

    @attribute(dtype=('str',), max_dim_x=197, label="VCC-receptor map")
    def vccToReceptor(self):
        return self._vcc_to_receptor

    @attribute(dtype=('str',), max_dim_x=197, label="Receptor-VCC map")
    def receptorToVcc(self):
        # We re-use the same one-to-one, arbitrary mapping
        return self._vcc_to_receptor

    (reportVCCAdminMode, reportVCCHealthState,
     reportVCCState, reportVCCSubarrayMembership) = _make_mock_reporting_attributes('VCC', 197, 'max_vccs')

    (reportFSPAdminMode, reportFSPHealthState,
     reportFSPState, reportFSPSubarrayMembership) = _make_mock_reporting_attributes('FSP', 27, 'max_fsps')

    @property
    def max_vccs(self):
        return self._max_capabilities['VCC']

    @property
    def max_fsps(self):
        return self._max_capabilities['FSP']

    def init_device(self):
        SKAMaster.init_device(self)
        self.set_state(tango.DevState.INIT)
        self._subarrays = tango.Group("Subarrays")
        self._subarrays.add(list(self.CbfSubarrays))
        self._vcc_to_receptor = [f'{receptor}:{receptor}'
                                 for receptor in range(1, self.max_vccs + 1)]
        self.set_state(tango.DevState.STANDBY)

    is_On_allowed, On = _make_forwarding_command("On", 'STANDBY')
    is_Off_allowed, Off = _make_forwarding_command("Off", 'STANDBY')
    is_Standby_allowed, Standby = _make_forwarding_command("Standby", 'ON', forwards_to="Off")

def main():
    CbfMaster.run_server()

if __name__ == '__main__':
    main()