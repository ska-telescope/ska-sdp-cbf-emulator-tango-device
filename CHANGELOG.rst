###########
Change Log
###########

0.5.0
*****
 * Some changes and additions to permit installation in the SKA repository:
    - change of the this repository and package name.
    - addition of a .release file

 * Exposed more configuration options
   ``transmisson_num_channels_per_stream`` for multiple streams from the packetiser
   ``reader_num_repeats`` for the sending of multiple copies of the input time stamps
   ``reader_num_timestamps`` to read a limited number of timestamps if required.

 * Added ``configureDelayExpected`` attribute
   needed by CSP to successfully wait
   for long-running transitions from
   ``Configuring`` to ``Ready``.

0.4.3
*****

 * Implemented the ``RemoveReceptors`` command
   on the ``CbfSubarray`` device.
   The lack of this command
   was causing the overall system (i.e., SKAMPI)
   to not come down fully cleanly on shutdown.
 * Made ``receptors`` attributes of ``CbfSubarray`` read-write
   to avoid a `pytango bug <https://gitlab.com/tango-controls/pytango/-/issues/230>`_,
   which in turn uncovered a problem with the ``MidCspSubarrayBase``.

0.4.2
*****

 * Fixed the state machine of the mock ``CbfMaster`` device.
   The real device doesn't follow
   the state machine of the base ``SKADevice`` class,
   which led to confusions.

0.4.1
*****

 * Implemented the ``EndScan`` command
   on the ``CbfSubarray`` device.
   It is similar to the ``Abort`` command
   in that it stops any data sending
   that could be in progress.

0.4
***

 * Added a new ``CbfMaster`` device implementation
   that mimics the real device's behavior
   to the minimum extent possible.
   In particular it doesn't require the VCC and FSP devices
   to be present in the system.`
 * Added a new ``mid-cbf-emulated`` Helm chart
   that brings up one ``CbfMaster`` and three ``CbfSubarray`` devices
   to form a CBF subsystem.
   This is a shadow of the real ``mid-cbf`` chart,
   but simplified to make it possible
   to work with an emulated system.
 * Simplified repository dev/test infrastructure.

0.3
***

 * Adjusted Tango Device implementation
   to better mimic the Mid-CBF CbfSubarray device
   by offering the same commands and attributes.
   This (plus the other changes described below)
   make our Tango Device a drop-in replacement
   for the Mid-CBF CbfSubarray.
 * Aligned ``Configure`` JSON payload parsing
   to be able to deal with Mid-CBF-like configuration strings.
 * Renamed device class to ``CbfSubarray``
   for easier exchange with Mid-CBF's ``CbfSubarray`` device.
 * Added support for new ``time_interval`` emulator option
   to send data at expected integration times.
 * Updated dependency on cbf-sdp-emulator >= 1.5.
   This update brings a number of benefits,
   like time interval support and a dependency on python-casacore
   instead of OSKAR.
 * Updated dependency on ska-tango-base
   (rather than lmcbaseclasses, which is now obsolete).
 * Fix packaging of python package, now a proper distribution file is generated.
 * Improved hostname resolution logic and logging.
 * Simplified internal configuration of emulator sender.
 * Simplified device initialisation logic,
   avoiding unnecessary state transition calls.
 * Added automatic python package publication to Nexus.
 * Added publication of versioned Docker images to Nexus.

0.2
***

 * The ``Configure`` command has been adjusted
   to receive a JSON payload
   as agreed during the `PSO-944 discussions <https://confluence.skatelescope.org/x/MYKZBw>`_.
 * While in ``CONFIGURING`` state
   the device will go to a retry loop
   to resolve the output hostname into an IP.
   If after all retries resolution cannot be done
   the device will move to ``FAULT``.

0.1
***

 * The device is now configured via the ``Configure`` command
   instead of using custom Tango attributes.
 * Fixed some unit tests that were proving the wrong thing.
 * Prevent crashes when the device is being deleted
   by properly stopping all background tasks.

0.0.1
*****

 * New SKASubarray-based Tango Device implemented
   under ``ska.cbf.tango_device.CBFSDPIntEmu``.
   The device wraps the `cbf-sdp-emulator <https://gitlab.com/ska-telescope/cbf-sdp-emulator>`_ sender code,
   sending data over SPEAD2 upon ``Scan``.
 * The device is configured via Tango-exposed attributes.
   Among the things that can be configured are
   the target host/port, transfer rate,
   and the Measurement Set to send.
 * The Measurement Set to be sent is retrieved upon ``Configure``,
   in case it isn't locally present.
