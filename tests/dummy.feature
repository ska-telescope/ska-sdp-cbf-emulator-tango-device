Feature:

	Scenario: LMC device can be configured
		Given An LMC device controlling the CBF emulator in OFFLINE state
		Then A configuration can be passed to the CBF emulator

	Scenario: LMC device can be started
		Given An LMC device controlling the CBF emulator in READY state
		Then The CBF emulator can be started

	Scenario: LMC device fails to start with wrong configuration
		Given An LMC device controlling the CBF emulator in OFFLINE state
		When The device is wrongly configured
		Then The CBF emulator fails to send data

	Scenario: LMC device fails to configure with unresolvable hostname
		Given An LMC device controlling the CBF emulator in OFFLINE state
		When The device is configured with an unresolvable hostname
		Then The CBF emulator goes to FAULT state

	Scenario: LMC device can be stopped
		Given An LMC device controlling the CBF emulator in SCANNING state
		Then The CBF emulator can be <action>

	Examples:
		| action  |
		| stopped |
		| aborted |

	Scenario: LMC device can be aborted before a scan
		Given An LMC device controlling the CBF emulator in READY state
		Then The CBF emulator can be aborted

	Scenario: LMC device can be aborted after a scan
		Given An LMC device controlling the CBF emulator in READY state
		When The CBF emulator is started
		And The CBF emulator finishes its current scan
		Then The CBF emulator can be aborted

	Scenario: LMC device fails to be configured
		Given An LMC device controlling the CBF emulator in OFFLINE state
		When An incorrect configuration is passed to the CBF emulator
		Then The CBF emulator goes to FAULT state

	Scenario: Receptors can be removed
		Given An LMC device controlling the CBF emulator in IDLE state
		When The device has all its receptors removed
		Then The CBF emulator goes to EMPTY state
