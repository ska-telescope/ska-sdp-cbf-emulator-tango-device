Feature: Master device behavior

	Scenario: Master device can move between states
		Given A Master device in <start_state> state
		Then It can be moved to <end_state> state
		And Its VCC-receptor mapping can be read
		And Its reporting attributes can be read

		Examples:
		| start_state | end_state |
		| STANDBY     | ON        |
		| STANDBY     | OFF       |
		| ON          | STANDBY   |
