# -*- coding: utf-8 -*-
"""
Some simple unit tests of the CBF-SDP Interface Emulator device, exercising the device from
the same host as the tests by using a DeviceTestContext.
"""
import json
import os
import shutil
import tempfile
import time

import numpy as np
import pytest
from pytest_bdd import parsers
from pytest_bdd.scenario import scenarios
from pytest_bdd.steps import given, then, when
from ska_tango_base.control_model import ObsState
from tango import DevState
from tango.test_utils import DeviceTestContext

from ska.cbf.tango_device import CbfSubarray, EXPECTED_CONFIGURATION_DELAY


scenarios('dummy.feature')

@pytest.fixture
def device_proxy():
    with DeviceTestContext(CbfSubarray, host='127.0.0.1', process=True, timeout=30) as device:
        yield device


@given(
    parsers.parse("An LMC device controlling the CBF emulator in {state} state"),
    target_fixture="device"
)
def device(device_proxy, state):
    assert state in ('OFFLINE', 'IDLE', 'SCANNING', 'READY')
    assert device_proxy.configurationDelayExpected == EXPECTED_CONFIGURATION_DELAY
    assert device_proxy.configureDelayExpected == EXPECTED_CONFIGURATION_DELAY
    if state != 'OFFLINE':
        _to_idle(device_proxy)
        if state != 'IDLE':
            _configure_device(device_proxy)
            if state != 'READY':
                _start_device(device_proxy)
    return device_proxy

@when("The device is wrongly configured")
def get_wrongly_configured_device(device):
    _configure_device(device, outputPort=[[0, 100000]])

@when("The device is configured with an unresolvable hostname")
def get_device_configured_with_unresolvable_hostname(device):
    _configure_device(
        device, assert_successful_configuration=False,
        outputHost=[[0, 'this-host-does-not-exist']],
        name_resolution_max_tries=3, name_resolution_retry_period=0.1
    )

@when("The CBF emulator is started")
@then("The CBF emulator can be started")
def start_device(device):
    _start_device(device)


@when("The CBF emulator finishes its current scan")
def device_finishes_current_scan(device):
    assert device.obsState in (ObsState.READY, ObsState.SCANNING)
    while device.sending:
        time.sleep(0.5)
    assert device.obsState == ObsState.READY
    return device

@then("A configuration can be passed to the CBF emulator")
def can_be_configured(device):
    _configure_device(device, transmission_method='doesnt exist')

@when("An incorrect configuration is passed to the CBF emulator")
def can_be_configured_incorrectly(device):
    # We use a temporary directory to make sure we trigger a download
    # (that fails) regardless of whether other tests have executed
    tmpdir = tempfile.mkdtemp()
    transmission_source_url = 'does not exist'
    transmission_source = os.path.join(tmpdir, 'does not exist')
    _configure_device(
        device, assert_successful_configuration=False,
        transmission_source=transmission_source,
        transmission_source_url=transmission_source_url
    )
    shutil.rmtree(tmpdir)

@when("The device has all its receptors removed")
def remove_all_receptors(device):
    assert np.all(device.receptors == [1, 2, 3])
    device.RemoveReceptors(device.receptors)
    assert len(device.receptors) == 0

@then(
    parsers.parse("The CBF emulator goes to {state} state")
)
def device_in_fault_state(device, state):
    assert device.obsState == getattr(ObsState, state)

@then("The CBF emulator fails to send data")
def fails_to_send_data(device):
    _start_device(device)
    while device.sending:
        time.sleep(0.5)
    assert device.obsState == ObsState.FAULT

@then("The CBF emulator can be <action>")
def stop_device(device, action):
    if action == 'aborted':
        _abort_device(device)
    else:
        _stop_device(device)

@then("The CBF emulator can be aborted")
def abort_device(device):
    _abort_device(device)

# Helper functions to move a device through different states
def _to_idle(device):
    device.Init()
    assert device.State() == DevState.OFF
    assert device.obsState == ObsState.EMPTY
    device.On()
    assert device.State() == DevState.ON
    assert device.obsState == ObsState.EMPTY
    assert len(device.receptors) == 0
    device.AddReceptors([1, 2, 3])
    assert device.State() == DevState.ON
    assert device.obsState == ObsState.IDLE
    assert np.all(device.receptors == [1, 2, 3])

def _configure_device(device, assert_successful_configuration=True, **configuration_args):
    if device.obsState != ObsState.IDLE:
        _to_idle(device)
    # Send data fast always so tests don't take long
    configuration_args['transmission_rate'] = 100 * 1024 * 1024
    configuration_args['integrationTime'] = -1
    configuration_args = {'id': 0, 'fsp': [configuration_args]}
    device.Configure(json.dumps(configuration_args))
    assert device.State() == DevState.ON
    while device.obsState == ObsState.CONFIGURING:
        time.sleep(0.5)
    if assert_successful_configuration:
        assert device.obsState == ObsState.READY

def _start_device(device):
    assert device.obsState == ObsState.READY
    device.Scan(1)
    assert device.State() == DevState.ON
    assert device.sending

def _abort_device(device):
    device.Abort()
    assert device.State() == ObsState.EMPTY
    assert not device.sending

def _stop_device(device):
    device.EndScan()
    assert device.obsState == ObsState.READY
    assert not device.sending
